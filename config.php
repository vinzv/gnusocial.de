<?php
if (!defined('GNUSOCIAL')) { exit(1); }

# Site
$config['site']['name'] = 'GNUsocial.de';
$config['site']['server'] = 'gnusocial.de';
$config['site']['path'] = '';
$config['site']['fancy'] = true;
$config['site']['ssl'] = 'always';
$config['site']['theme'] = 'neo-quitter';
$config['public']['localonly'] = false;
$config['profile']['changenick'] = true;
$config['profile']['delete'] = true;


# Qvitter
$config['site']['qvitter']['enabledbydefault'] = true;
$config['site']['qvitter']['defaultlinkcolor'] = '#a22430';
$config['site']['qvitter']['sitebackground'] = 'img/bokeh_colourful_city_night.jpg';
$config['thumbnail']['maxsize'] = 3000;
$config['site']['qvitter']['timebetweenpolling'] = 3000;


# DB
$config['db']['database'] = 'mysqli://USER:PASS@unix(/var/run/mysqld/mysqld.sock)/DATABASE';
$config['db']['type'] = 'mysql';
$config['db']['schemacheck'] = 'script';


# Queue
$config['queue']['daemon'] = true;
$config['queue']['enabled'] = true;
$config['queue']['subsystem'] = 'db';
$config['daemon']['piddir'] = '/var/run/';
$config['daemon']['user'] = 'www-data';
$config['daemon']['group'] = 'www-data';
$config['emailpost']['enabled'] = false;
$config['oldschool']['enabled'] = true;

# nofollow
$config['nofollow']['external'] = FALSE;
$config['nofollow']['subscribers'] = FALSE;
$config['nofollow']['members'] = FALSE;
$config['nofollow']['peopletag'] = FALSE;


# Logs
$config['site']['logdebug'] = true;
$config['site']['logfile'] = '/var/www/gnusocial_de_logs/gnusocial_de.log';
addPlugin('LogFilter', array(
        'priority' => array(LOG_DEBUG => false)
        ));


# Mail backend
$config['mail']['notifyfrom'] = 'noreply@gnusocial.de';
$config['mail']['domain'] = 'gnusocial.de';
$config['mail']['backend'] = 'mail';
#$config['mail']['params'] = array(
#       'host' => 'mail.gnusocial.de',
#       'auth' => true,
#       'username' => 'REDACTED',
#       'password' => 'REDACTED',
#       'port' => 25);


# Plugins
addPlugin('Activity', array(
        'StartFollowUser' => false,
        'StopFollowUser' => false,
        'JoinGroup' => false,
        'LeaveGroup' => false,
        'StartLike' => false,
        'StopLike' => false,
        'Favorite' => false,
        'Delete' => false));
addPlugin('Autocomplete');
addPlugin('ClientSideShorten');
addPlugin('ChooseTheme');
#addPlugin('Diaspora');
addPlugin('EmailReminder');
addPlugin('FeedPoller');
addPlugin('GroupPrivateMessage');
addPlugin('LRDD');
addPlugin('ModPlus');
#addPlugin('Nominatim');
addPlugin('OStatus');
addPlugin('OpenExternalLinkTarget');
addPlugin('Qvitter');
addPlugin('RequireValidatedEmail');
addPlugin('Statistics');
addPlugin('StoreRemoteMedia');
addPlugin('TabFocus');
#addPlugin('TwitterBridge');
#       $config['twitterimport']['enabled'] = false;
#       $config['integration']['source'] = 'GNUsocial.de';
#       $config['twitter']['global_consumer_key'] = 'REDACTED';
#       $config['twitter']['global_consumer_secret'] = 'REDACTED';
addPlugin('WebFinger');

# Deactivated
unset($config['plugins']['default']['Mapstraction']);
unset($config['plugins']['default']['OpenID']);
unset($config['plugins']['default']['RSSCloud']);
unset($config['plugins']['default']['WikiHashtags']);
unset($config['plugins']['default']['TightUrl']);
unset($config['plugins']['default']['SimpleUrl']);
unset($config['plugins']['default']['PtitUrl']);
unset($config['plugins']['default']['Geonames']);
unset($config['plugins']['core']['OpportunisticQM']);
unset($config['plugins']['core']['Cronish']);
