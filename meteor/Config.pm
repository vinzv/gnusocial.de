# Trimmed for convenience

'IP address for controller server (leave empty for all local addresses)',
        ControllerIP                    => '',

'Port number for controller connections',
        ControllerPort                  => 4671,
...
'IP address for subscriber server (leave empty for all local addresses)',
        SubscriberIP                    => '',

'Port number for subscriber connections',
        SubscriberPort                  => 4670,
